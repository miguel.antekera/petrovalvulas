jQuery(document).ready(function($) {
  ///////////////////////////////////////////
  /// FORM VALIDATION
  ///////////////////////////////////////////

  $.validator.addMethod("lettersonly", function(value, element) {
    return this.optional(element) || /^[a-z\s]+$/i.test(value);
  }, "Ingresa solo letras.");

  $.validator.addMethod( "regexNumber", function(value, element) {
        return this.optional(element) || /^[+-]?\d+$/i.test( value );
    }, "Ingresa solo números."
  );

  $('.mobile').focus(function(event) {
    if($(this).val() == ''){
      $(this).val('+58');
    } else {
    }
  });

  $(".validate-form").validate({

    ignore: [],
    errorPlacement: function(error, element) {
      error.appendTo(element.parents(".js-form-wrapper"));
    },
    rules: {
      name: {
        required: true,
        lettersonly: true,
        maxlength: 60,
      },
      mobile: {
        required: true,
        regexNumber: true,
      },
      email: {
        required: true,
        email: true,
        maxlength: 60,
      },
      message: {
        required: true,
        maxlength: 1000,
      },
      // "hiddenRecaptcha": {
      //   required: function() {
      //     if(grecaptcha.getResponse() == '') {
      //         return true;
      //     } else {
      //         return false;
      //     }
      //   }
      // }
    },
    messages: {
      name: {
        required: "Debes ingresar tu nombre y apellido.",
        lettersonly: "Debes ingresar tu nombre y apellido.",
        maxlength: "Excede el máximo de carácteres permitidos.",
      },
      mobile: {
        required: "Debes ingresar un teléfono.",
        number: "Solo acepta números.",
      },
      email: {
        required: "Debes ingresar tu e-mail.",
        email: "E-mail inválido.",
        maxlength: "Excede el máximo de carácteres permitidos.",
      },
      message: {
        required: "Por favor ingresa tu comentario.",
        maxlength: "Excede el máximo de carácteres permitidos.",
      },
      // hiddenRecaptcha: "Debes validar este campo para continuar.",
    },

    submitHandler: function() {
      $(".validate-form .btn").prop('disabled', true);
      var form = $('.validate-form');
      var name = $("#name").val();
      var email = $("#email").val();
      var mobile = $("#mobile").val();
      var message = $("#message").val();

      $.ajax({
        type: "POST",
        url: "form-process.php",
        data: "name=" + name + "&email=" + email + "&mobile=" + mobile + "&message=" + message,
        success: function(response) {
          $(".validate-form")[0].reset();
          $(".validate-form .form-control").val('');
          $(".alert").removeClass("collapse").addClass("alert-success").html(response);
          $(".validate-form .btn").prop("disabled",true);
          console.log(response);
        },
        error: function(response) {
          $(".alert").removeClass("collapse").addClass("alert-danger").html(response);
          $(".validate-form .btn").prop("disabled",false);
          console.log(response);
        },
      });
    }

  });

});
